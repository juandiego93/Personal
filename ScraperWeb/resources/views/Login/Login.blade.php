@extends('layouts.home')

{!!Html::style(asset('./css/Application/home/Login_style.css'))!!}

@section('content')
<div class="container-fluid contenido notPadding" id="parrafo1" ng-controller="homeController" > 
	<div class="parrafo">
		<p class="inicioCampanas" id="inicioCampanas">
			Tus campañas con influencers en un solo lugar. 
			<br>
			Inspírate. Inicia la campaña. Lleva su seguimiento.
		</p>

		<div class="arrowDown"  id="ocultar" ng-click="dowButon()"></div>
		<div class="equisLogin" ng-click="gotoBottom()"></div>
		<div class="autenticacion" >
			Hola
		</div>
	</div>
	
</div>
@stop