<!--* ----------------------------------------------------------------------------
 *                         Todos los derechos Reservados 

 *  ----------------------------------------------------------------------------
 *	Nombre:        home.blade.php
 *	Ruta:         /resources/views/layouts/home.blade.php
 *	Descripción:  Vista principal de la app
 *	Fecha:        15/07/2018 
 *  Autor:        Juan Diego Osorio Castrillon.
 *  Versión:      1.0 
 *  ----------------------------------------------------------------------------
 *	 							Histórico de cambios
 *  ----------------------------------------------------------------------------
 *	  Fecha           Autor               Descripción
 *  ----------------------------------------------------------------------------
 *  15/07/2018    JuanDiegoOC   Creación del archivo javascript.
 *  ----------------------------------------------------------------------------
 -->

 <!DOCTYPE html>
 <html lang="es">
 <head>
 	<!--Meta tags principales, carga inicial -->
 	<meta name="csrf-token" content="@yield('token')">
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<meta http-equiv="x-ua-compatible" content="ie-edge">

 	<!-- Carga de Scripts Principales -->

	<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->

	<!--   JQuery 3.2.1 	-->
 	{!!Html::script(asset('./bower_components/jquery-3.2.1/jquery-3.2.1.js'))!!}

 	

 	<!-- Angular JS -->
 	{!!Html::script(asset('./bower_components/angular-1.6.6/angular.min.js'))!!}

 	<!-- Angular - route-->
 	{!!Html::script(asset('./bower_components/angular-1.6.6/angular-route.js'))!!}

 	<!-- Carga de configuracion inical AngularJS -->
 	{!!Html::script(asset('js/Aplication/appScrapWeb.js'))!!}

	<!-- Script general del sitio -->
 	{!!Html::script(asset('js/site_script.js'))!!}

 	<!-- Bootstrap -->
	{!!Html::style(asset('./bower_components/bootstrap-3.3.7/css/bootstrap.min.css'))!!} 
	
	<!-- Bootstrap Material Design -->
	{!!Html::style(asset('./bower_components/bootstrap_material/css/bootstrap-material-design.css'))!!}

 	{!!Html::script(asset('./bower_components/bootstrap-3.3.7/js/bootstrap.min.js'))!!}

	{!!Html::style(asset('./css/Application/home/Home_style.css'))!!}

 	{!!Html::style(asset('./css/Application/Navbar/Navbar_style.css'))!!}

 	{!!Html::script(asset('js/Aplication/Home/crudHome.js'))!!}
 </head>
	 <body ng-app="appScrapWeb" class="containerBody" id="elemento">  

	 	
		 @include('Navbar.Navbar')
 		

	 	<div class="container notPadding notMargin"> 
	 		@yield('content') 
	 	</div>  
	 </body>
 </html>