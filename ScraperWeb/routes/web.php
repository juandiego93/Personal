<?php

/* ----------------------------------------------------------------------------
 *                         Todos los derechos Reservados 

 *  ----------------------------------------------------------------------------
 *	Nombre:        home.blade.php
 *	Ruta:         /app/Http/Controllers/LogicaNegocio/ControladorUsuario.php
 *	Descripción:  Archivo Principal de las rutas
 *	Fecha:        15/07/2018 
 *  Autor:        Juan Diego Osorio Castrillon.
 *  Versión:      1.0 
 *  ----------------------------------------------------------------------------
 *	 							Histórico de cambios
 *  ----------------------------------------------------------------------------
 *	  Fecha           Autor               Descripción
 *  ----------------------------------------------------------------------------
 *  15/07/2018    JuanDiegoOC     Creación del archivo rutas.
 *  ----------------------------------------------------------------------------
 */


Route::get('/', function () {
    return view('welcome');
});


Route::get('home','LogicaNegocio\ControladorUsuario@index'); 