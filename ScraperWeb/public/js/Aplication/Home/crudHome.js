/** ----------------------------------------------------------------------------
 *                            Todos los derechos Reservados
 *                           	FREEDOMCENTER S.A.S 2018
 *  ----------------------------------------------------------------------------
 *	Nombre:       crudHome.js
 *	Ruta:         /public/js/Aplication/Home/crudHome.js
 *	Descripción:  Estilo modulo de administracion
 *	Fecha:        01/08/2018
 *  Autor:        Juan Diego Osorio Castrillón
 *  Versión:      1.0 
 *  ----------------------------------------------------------------------------
 *	 							Histórico de cambios
 *  ----------------------------------------------------------------------------
 *	  Fecha           Autor        Descripción
 *  ----------------------------------------------------------------------------
 *  01/08/2018     lfriveraco   Creación de archivo
 *  ----------------------------------------------------------------------------
 */

appScrapWeb.controller('homeController', ['$scope', '$location', '$anchorScroll', function($scope, $location,$anchorScroll, $http){

 /*|---------------------------------------------------------------------------------------------------------------------------------|
  *|---------------------------------------------------------|Objetos|---------------------------------------------------------------|
  *|---------------------------------------------------------------------------------------------------------------------------------|
  */



 /*|---------------------------------------------------------------------------------------------------------------------------------|
  *|---------------------------------------------------------|Funciones|-------------------------------------------------------------|
  *|---------------------------------------------------------------------------------------------------------------------------------|
  */

	$(window).scroll(function(){
				if ($(window).scrollTop()>90) {  
  
					        $("#parrafo").fadeOut(1500); 
					        $("#inicioCampanas").fadeOut(1500); 
				} 

				if($(window).scrollTop() == 0){
					//$("#parrafo").fadeIn(1500); 
			        $("#inicioCampanas").fadeIn(1000);
			        $(".inicioCampanas").addClass('display');  
					//$("#inicioCampanas").addClass('display');  
					//$("#parrafo").fadeIn(1500); 
				}
	});



 	//Funcion para ocultar el Scroll al moverse por la pagina
	$scope.ocultarElementos = function(){ 
		 	$('.containerBody').fullpage();
	}

	$scope.gotoBottom = function() {
      $location.hash();

      // call $anchorScroll()
      $anchorScroll();
    };

    $scope.dowButon = function(){
    	$anchorScroll.yOffset = 100;
    };
}]);