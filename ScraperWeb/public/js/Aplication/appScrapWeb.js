/** ----------------------------------------------------------------------------
 *                         Todos los derechos Reservados 

 *  ----------------------------------------------------------------------------
 *	Nombre:        appScrapWeb.js
 *	Ruta:         /public/js/Controllers/appScrapWeb.js
 *	Descripción:  Script de carga para la APP angularJS
 *	Fecha:        15/07/2018
 *  Autor:        Juan Diego Osorio Castrillon.
 *  Versión:      1.0 
 *  ----------------------------------------------------------------------------
 *	 							Histórico de cambios
 *  ----------------------------------------------------------------------------
 *	  Fecha           Autor               Descripción
 *  ----------------------------------------------------------------------------
 *  15/07/2018    JuanDiegoOC   Creación del archivo javascript.
 *  ----------------------------------------------------------------------------
 */


var appScrapWeb = angular.module('appScrapWeb',[]).config(function($interpolateProvider, $httpProvider){
	$interpolateProvider.startSymbol('{$');
	$interpolateProvider.endSymbol('$}');

	$httpProvider.defaults.xsrfCookieName='csrftoken';
	$httpProvider.defaults.xsrfHeaderName='X-CSRF-TOKEN';
});

appScrapWeb.service('appScrapWebService', function(){ 
  this.errors =[];
});