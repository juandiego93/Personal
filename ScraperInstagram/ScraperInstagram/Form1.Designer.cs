﻿namespace ScraperInstagram
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.wc1 = new EO.WinForm.WebControl();
            this.webView1 = new EO.WebBrowser.WebView();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // wc1
            // 
            this.wc1.BackColor = System.Drawing.Color.White;
            this.wc1.Location = new System.Drawing.Point(-1, -1);
            this.wc1.Name = "wc1";
            this.wc1.Size = new System.Drawing.Size(1075, 504);
            this.wc1.TabIndex = 0;
            this.wc1.Text = "webControl1";
            this.wc1.WebView = this.webView1;
            this.wc1.Click += new System.EventHandler(this.wc1_Click);
            // 
            // webView1
            // 
            this.webView1.ObjectForScripting = null;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(389, 516);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 551);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.wc1);
            this.Name = "Form1";
            this.Text = "s";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private EO.WinForm.WebControl wc1;
        private EO.WebBrowser.WebView webView1;
        private System.Windows.Forms.Button button1;
    }
}

