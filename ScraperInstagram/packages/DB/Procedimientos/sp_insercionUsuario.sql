USE `mydb`;
DROP procedure IF EXISTS `sp_insercionUsuario`;

DELIMITER $$
USE `mydb`$$
CREATE PROCEDURE `sp_insercionUsuario` (
		_usuario VARCHAR(50),
		_nombreUsuario VARCHAR(50), 
        _fotoPerfil VARCHAR(500), 
        _numeroPublicaciones VARCHAR(12),
		_numeroSeguidores VARCHAR(12),
        _numroSeguidos VARCHAR(12),
        _biografia VARCHAR(120),
        _ciudadUsuario VARCHAR(45))
BEGIN
INSERT INTO tus_usuarios( tus_usuario, 
						  tus_nombreUsuario, 
                          tus_fotoPerfil, 
                          tus_numeroPublicac,
                          tus_numeroSeguidores,
                          tus_numeroSeguidos, 
                          tus_biografia, 
                          tus_fecha,
                          tus_ciudadUsuario) 
VALUES (_usuario,
		_nombreUsuario, 
        _fotoPerfil, 
        _numeroPublicaciones,
		_numeroSeguidores,
        _numroSeguidos,
        _biografia,
        NOW(),
        _ciudadUsuario);
END$$

DELIMITER ;