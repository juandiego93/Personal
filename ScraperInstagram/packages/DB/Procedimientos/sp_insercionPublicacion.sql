USE `mydb`;
DROP procedure IF EXISTS `sp_insercionPublicacion`;

DELIMITER $$
USE `mydb`$$
CREATE PROCEDURE `sp_insercionPublicacion` (_urlPublicacion VARCHAR(120),
		_fechaPublicacion VARCHAR(45),
        _autorPublicacion VARCHAR(45),
        _tipoPublicacion VARCHAR(12),
        _attbtoPublicacion VARCHAR(120),
        _textoPublicacion VARCHAR(120),
        _likes VARCHAR(12),
        _reproducciones VARCHAR(45),
        _ubicacion VARCHAR(50),
        _nombreUsuario VARCHAR(45))
BEGIN
INSERT INTO tpb_publicacionusuario(tpb_urlPublicacion,
								   tpb_fechaPublicacion,  
								   tpb_autorPublicacion,
                                   tpb_tipoPublicacion,
								   tpb_contenido,  
                                   tpb_textoPublicacion,
                                   tpb_likes,  
                                   tpb_numeroReproducciones,  
                                   tpb_ubicacion, 
                                   fk_tus_user) 
VALUES (_urlPublicacion,
		_fechaPublicacion,
        _autorPublicacion,
        _tipoPublicacion,
        _attbtoPublicacion,
        _textoPublicacion,
        _likes,
        _reproducciones,
        _ubicacion,
        _nombreUsuario);
END$$

DELIMITER ;
